import Vue from "vue"
// eslint-disable-next-line import/extensions
import App from "../App"

new Vue({
  render: h => h(App),
}).$mount("#app")

if (process.env.NODE_ENV !== "production" && module.hot) {
  module.hot.accept()
}
