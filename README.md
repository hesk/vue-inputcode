# vue-codeinput
Great pincode input component for Vue.js applications.

![vue-codeinput](https://repository-images.githubusercontent.com/210697453/85bbb480-dffa-11e9-90a3-f4114739ed91)

Demo on [GitHub Pages](https://seokky.github.io/vue-codeinput/)

## Features
- configurable length (symbols count)
- override-friendly styles
- auto moving focus when filling
- auto moving focus when deleting
- auto selecting cell content on focusing
- call for native numeric keyboard on mobiles
- optional secure mode (password input type)
- character preview on typing (configurable duration)

## Attention!

Styles that component have are written just for demo. But that styles are override-friendly, so you can write any styles you want.

## Usage

```
  npm i --save vue-codeinput
```
or with yarn
```
  yarn add vue-codeinput
```

Then in any component:

```
import PincodeInput from 'vue-codeinput';
// The name can be different depending on your desire
```

```
<div class="input-wrapper">
  <PincodeInput
        ref="fillingpincode"
        v-model="code"
        placeholder="-"
        :length="5"
        @fillcomplete="(val)=>{messsage=`now finished ${val}`}"
  />
</div>
```

**Attention**: you should use _'input.vue-codeinput'_ instead _'.vue-codeinput'_ in order to rule specificity was higher

```
<style>
div.vue-codeinput-wrapper {
  // any styles you want for wrapper
}

input.vue-codeinput {
  // any styles you want for each cell
}
<style>
```

## Props

- **length** (symbols count)
  - type: Number
  - default: 4

- **autofocus** (auto focus first cell)
  - type: Boolean
  - default: true

- **secure** (password input type)
  - type: Boolean
  - default: false

- **characterPreview** (preview character on typing)
  - type: Boolean
  - default: true

- **previewDuration** (duration of character preview)
  - type: Number
  - default: 300

## ToDo

- configure husky
- write unit tests for v0.2.0 (characterPreview and previewDuration props)
